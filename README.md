Babygame on OpenShift Express
============================
This git repository helps you get up and running quickly w/ a Babygame installation
on OpenShift Express.  The backend database is MySQL and the database name is the
same as your application name (using $_ENV['OPENSHIFT_APP_NAME']).


Manual install on OpenShift
---------------------------
Create a php-5.3 application

    rhc app create -t php-5.3 babygame

Add MySQL support to your application

    rhc cartridge add mysql-5.1 -a babygame

Pull in the babygame project code

    cd babaygame
    
    git remote add upstream -m master git://github.com/eschabell/openshift-babygame.git
    
    git pull -s recursive -X theirs upstream master
    
    git push


